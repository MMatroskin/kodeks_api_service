import configparser
from os.path import join
from settings import ROOT_DIR, CONFIG


config = configparser.ConfigParser()

# app settings
config_path = join(ROOT_DIR, CONFIG)
config.read(config_path)

MESSAGE_SUCCESS = config.get("messages", "message_success")
MESSAGE_ERROR = config.get("messages", "message_error")
MESSAGE_404 = 'Недоступно'
MESSAGE_BAD_QUERY = 'Неверный запрос'
HEADERS = {}
